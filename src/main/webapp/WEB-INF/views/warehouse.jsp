<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="mytags" tagdir="/WEB-INF/tags" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Warehouse - ${warehouseDto.name}</title>
        <style>
            <%@include file="/WEB-INF/css/style.css"%>
            <%@include file="/WEB-INF/css/navigation_style.css"%>
        </style>
    </head>

    <body class="Warehouse">
        <mytags:navbar/>

        <div class="Warehouse">
        <h1>Warehouse - ${warehouseDto.name}</h1>
        <p>${warehouseDto.description}</p>
        <p>Status:
        <c:if test="${warehouseDto.enabled}">
             <b class="Green">Enabled</b><br><br>
                <c:if test="${pageContext.request.isUserInRole('ADMIN')}" >
                    <a href="${contextPath}/warehouse/${warehouseDto.id}/disable">Click here to disable</a>
                </c:if>
        </c:if>

        <c:if test="${!warehouseDto.enabled}">
            <b class="Red">Disabled</b> <br><br>
            <c:if test="${pageContext.request.isUserInRole('ADMIN')}" >
                <a href="${contextPath}/warehouse/${warehouseDto.id}/enable" >Click here to enable</a>
            </c:if>
        </c:if>
        </p>

            <br>
            <br>
            <br>
        <form method="POST" action="${contextPath}/warehouse/uploadFile" enctype="multipart/form-data">
            <table>
                <tr>
                    <td><label path="file">Select a file to upload:</label></td>
                </tr>
                <tr>
                    <td><input type="file" name="file" /></td>
                </tr>
                <tr>
                    <input type="hidden" name="id" value="${warehouseDto.id}"/>
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    <td><input type="submit" value="Upload new job" /></td>
                </tr>
            </table>
        </form>

            <br>
            <br>
            <br>
        <c:if test="${pageContext.request.isUserInRole('ADMIN')}" >
            <a href="${contextPath}/warehouse/${warehouseDto.id}/remove">Remove warehouse and data!</a>
        </c:if>
            <br>
            <br>
            <br>

        <table>
            <tr>
                <th>Id:</th>
                <th>Name:</th>
                <th>Enabled:</th>
                <th>Click to enable/disable</th>
                <th>Click to run job now</th>
                <th>Click to remove job</th>
            </tr>
            <c:forEach items="${warehouseDto.jobs}" var="job">
                <tr>
                    <td>
                            ${job.id}
                    </td>
                    <td>
                            ${job.name}
                    </td>
                    <c:if test="${job.enabled}">
                        <td>
                                <b class="Green">Enabled</b>
                        </td>
                        <td>
                            <a href="${contextPath}/warehouse/${warehouseDto.id}/job/${job.id}/disable">Disable</a>
                        </td>

                    </c:if>
                    <c:if test="${!job.enabled}">
                        <td>
                            <b class="Red">Disabled</b>
                        </td>

                        <td>
                            <a href="${contextPath}/warehouse/${warehouseDto.id}/job/${job.id}/enable">Enable</a>
                        </td>
                    </c:if>

                    <td>
                        <a href="${contextPath}/warehouse/${warehouseDto.id}/job/${job.id}/run">Run now</a>
                    </td>
                    <td>
                        <a href="${contextPath}/warehouse/${warehouseDto.id}/job/${job.id}/remove">Remove</a>
                    </td>
                </tr>
            </c:forEach>
        </table>
        </div>
    </body>
</html>
