<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="mytags" tagdir="/WEB-INF/tags" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Create an account</title>
        <style>
            <%@include file="/WEB-INF/css/style.css"%>
            <%@include file="/WEB-INF/css/navigation_style.css"%>
        </style>

    </head>
    <body>
        <mytags:navbar/>
        <h1>Create your account</h1>


        <form:form action="${contextPath}/registration" method="POST" modelAttribute="registrationForm">
            <table>
                <tr>
                    <td>
                        <spring:bind path="username">
                            <form:errors path="username"/><br>
                            <form:input type="text" path="username" placeholder="Username" autofocus="true"/>
                        </spring:bind>
                    </td>
                </tr>
                <tr>
                    <td>
                        <spring:bind path="email">
                            <form:errors path="email"/><br>
                            <form:input type="email" path="email" placeholder="Email"/>
                        </spring:bind>
                    </td>
                </tr>
                <tr>
                    <td>
                        <spring:bind path="password">
                            <form:errors path="password"/><br>
                            <form:input type="password" path="password" placeholder="Password"/>
                        </spring:bind>
                    </td>
                </tr>
                <tr>
                    <td>
                        <spring:bind path="confirmPassword">
                            <form:errors path="confirmPassword"/><br>
                            <form:input type="password" path="confirmPassword" placeholder="Confirm your password"/>
                        </spring:bind>
                    </td>
                </tr>
                <tr>
                    <td><br>
                        <button type="submit">Submit</button>
                    </td>
                </tr>
            </table>
        </form:form>
    </body>
</html>
