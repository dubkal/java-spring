<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="mytags" tagdir="/WEB-INF/tags" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Add new warehouse</title>
    <style>
        <%@include file="/WEB-INF/css/style.css"%>
        <%@include file="/WEB-INF/css/navigation_style.css"%>
    </style>
</head>

<body>
<mytags:navbar/>


<form:form action="${contextPath}/saveWarehouse" method="POST" modelAttribute="addWarehouseForm">
    <h1>Add new warehouse</h1>
    <table>
        <tr>
            <td>
                <spring:bind path="name">
                    <form:errors path="name"/><br>
                    <form:input type="text" path="name" placeholder="Warehouse name" autofocus="true"/>
                </spring:bind>
            </td>
        </tr>
        <tr>
            <td>
                <spring:bind path="description">
                    <form:errors path="description"/><br>
                    <form:textarea type="text" path="description" placeholder="Description"/>
                </spring:bind>
            </td>
        </tr>
        <tr>
            <td>
                <br>
                <button type="submit">Submit</button>
            </td>
        </tr>
    </table>
</form:form>

</body>
</html>
