<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="mytags" tagdir="/WEB-INF/tags" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Welcome</title>
        <style>
            <%@include file="/WEB-INF/css/style.css"%>
            <%@include file="/WEB-INF/css/navigation_style.css"%>
        </style>
    </head>
    <body>
    <mytags:navbar/>
        <h1>Welcome ${pageContext.request.userPrincipal.name}</h1>
    </body>
</html>
