<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="mytags" tagdir="/WEB-INF/tags" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>List all warehouses</title>
    <style>
        <%@include file="/WEB-INF/css/style.css"%>
        <%@include file="/WEB-INF/css/navigation_style.css"%>
    </style>
</head>

<body>
<mytags:navbar/>

<h1>List all warehouses</h1>
    <table class="TableList">
        <thead>
        <tr>
            <td>
                ID
            </td>
            <td>
                Name
            </td>
            <td>
                Description
            </td>
            <td>
                Enabled
            </td>
            <td>
                View warehouse
            </td>
        </tr>
        </thead>
        <c:forEach items="${warehouseDtos}" var="warehouseDto">
            <tr>
                <td>
                        ${warehouseDto.id}
                </td>
                <td>
                        ${warehouseDto.name}
                </td>
                <td>
                        ${warehouseDto.description}
                </td>
                <c:if test="${warehouseDto.enabled}" >
                    <td class="Green">
                </c:if>
                    <c:if test="${!warehouseDto.enabled}" >
                        <td class="Red">
                    </c:if>
                        ${warehouseDto.enabled}
                </td>

                <td>
                    <a href="${contextPath}/warehouse/${warehouseDto.id}">View</a>
                </td>
            </tr>
        </c:forEach>
    </table>
</body>
</html>
