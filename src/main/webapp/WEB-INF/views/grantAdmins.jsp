<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="mytags" tagdir="/WEB-INF/tags" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Admin page - Grant Admins</title>
        <style>
            <%@include file="/WEB-INF/css/style.css"%>
            <%@include file="/WEB-INF/css/navigation_style.css"%>
        </style>
    </head>

    <body>
        <mytags:navbar/>
        <h1>Admin page - Grant Admins</h1>
        <form method="POST" action="${contextPath}/grantAdminsAction">
            <table>
                <thead>
                    <tr>
                        <td></td><td></td><td></td>
                        <td>
                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                            <button type="submit">Grant selected admins</button>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            ID
                        </td>
                        <td>
                            Username
                        </td>
                        <td>
                            Email
                        </td>
                        <td>
                            Grant Admin
                        </td>
                    </tr>
                </thead>
                <c:forEach items="${userDtos}" var="userDto">
                    <tr>
                        <td>
                            ${userDto.id}
                        </td>
                        <td>
                            ${userDto.username}
                        </td>
                        <td>
                            ${userDto.email}
                        </td>
                        <td>
                            <input type="checkbox" value="${userDto.id}" name="grantedAdminIds">
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </form>
    </body>
</html>
