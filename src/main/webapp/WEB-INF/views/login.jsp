<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="mytags" tagdir="/WEB-INF/tags" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Log in with your account</title>
    <style>
        <%@include file="/WEB-INF/css/style.css"%>
        <%@include file="/WEB-INF/css/navigation_style.css"%>
    </style>

</head>

<body>
    <mytags:navbar/>

    <c:if test = "${logout != null}">
        <span>
           <spring:message code="logout.message"/>
        </span>
    </c:if>


    <c:if test = "${error != null}">
        <span>
            <spring:message code="login.username.password.invalid"/>
        </span>
    </c:if>

    <c:if test = "${tokenUsed != null}">
        <span>
            <spring:message code="login.token.used"/>
        </span>
    </c:if>

    <c:if test = "${tokenExpired != null}">
        <span>
            <spring:message code="login.token.expired"/>
        </span>
    </c:if>


    <c:if test = "${tokenActivated != null}">
        <span>
            <spring:message code="login.token.activated"/>
        </span>
    </c:if>


    <c:if test = "${activateAccount != null}">
        <p>
            <spring:message code="login.email.activation.needed"/>
        </p>
    </c:if>

    <form method="POST" action="${contextPath}/login" class="center_element">
        <h1>Log in</h1>
        <table>
            <tr>
                <td>
                    <input name="username" type="text" placeholder="Username"
                           autofocus="true"/>
                </td>
            </tr>
            <tr>
                <td>
                    <input name="password" type="password" placeholder="Password"/>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    <button type="submit">Log In</button>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
