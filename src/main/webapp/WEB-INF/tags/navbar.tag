<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<table class="NavTable">
    <tr>
        <c:if test="${pageContext.request.userPrincipal.name == null}">
            <td>
                <a href="${contextPath}/login">Login</a>
            </td>
            <td>
                <a href="${contextPath}/registration">Registration</a>
            </td>
        </c:if>

        <c:if test="${pageContext.request.userPrincipal.name != null}">
            <td>
                <a href="${contextPath}/welcome">Welcome</a>
            </td>
            <td>
                <a href="${contextPath}/listWarehouses">Warehouses</a>
            </td>
            <c:if test="${pageContext.request.isUserInRole('ADMIN')}" >
                <td>
                    <a href="${contextPath}/addWarehouse">Add warehouse</a>
                </td>
                <td>
                    <a href="${contextPath}/grantAdmins">Grant administrators</a>
                </td>
                <td>
                    <a href="${contextPath}/revokeAdmins">Revoke administrators</a>
                </td>

            </c:if>
            <td>
                User:${pageContext.request.userPrincipal.name}
            </td>
            <td>
                <form id="logoutForm" method="POST" action="${contextPath}/logout">
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    <button type="submit">Logout</button>
                </form>
            </td>
        </c:if>
    </tr>
</table>