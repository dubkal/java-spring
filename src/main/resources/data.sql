/*Insert roles into database*/
INSERT INTO role (id, name) VALUES (1, 'ROLE_ADMIN');
INSERT INTO role (id, name) VALUES (2, 'ROLE_USER');

/*Insert admin into database and grant user and admin rights, password = nimda*/
INSERT INTO users (id, email, password, username, enabled)
  VALUES (1, 'admin@admin.com', '$2a$10$X5L7qChpNCHL3nK6Q9yUEe3bukiBJ8r2WpEpZOvHXkzRqe4Bf5Y9u', 'admin', true );
INSERT INTO users_roles (users_id, roles_id) VALUES (1, 1);
INSERT INTO users_roles (users_id, roles_id) VALUES (1, 2);

/*Insert user into database and grant user rights, password = user*/
INSERT INTO users (id, email, password, username, enabled)
  VALUES (2, 'user@user.com', '$2a$10$veS6ZElYs5MKI4M9HXxXz.Z1FLw0k244kvCqhOfYI2Vj9w9PBWcCG', 'user', true);
INSERT INTO users_roles (users_id, roles_id) VALUES (2, 2);
