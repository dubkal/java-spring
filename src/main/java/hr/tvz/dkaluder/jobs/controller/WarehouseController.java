package hr.tvz.dkaluder.jobs.controller;

import hr.tvz.dkaluder.jobs.dto.AddWarehouseForm;
import hr.tvz.dkaluder.jobs.facade.WarehouseFacade;
import hr.tvz.dkaluder.jobs.validator.AddWarehouseFormValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class WarehouseController {

    @Autowired
    private WarehouseFacade warehouseFacade;

    @Autowired
    private AddWarehouseFormValidator addWarehouseFormValidator;

    @GetMapping("/addWarehouse")
    public String addWarehouse(Model model){
        model.addAttribute("addWarehouseForm", new AddWarehouseForm());
        return "addWarehouse";
    }

    @PostMapping("/saveWarehouse")
    public String saveWarehouse(@ModelAttribute("addWarehouseForm") @Valid AddWarehouseForm addWarehouseForm,
                               BindingResult bindingResult){
        addWarehouseFormValidator.validate(addWarehouseForm, bindingResult);
        if (bindingResult.hasErrors()) {
            return "addWarehouse";
        }
        warehouseFacade.saveWarehouse(addWarehouseForm);

        return "redirect:/addWarehouse";
    }

    @GetMapping("/listWarehouses")
    public String listWarehouses(Model model){
        model.addAttribute("warehouseDtos", warehouseFacade.getWarehouseDtos());
        return "listWarehouses";
    }

    @GetMapping("/warehouse/{id}")
    public String warehouse(@PathVariable Long id, Model model){
        model.addAttribute("warehouseDto", warehouseFacade.getWarehouseDto(id));
        return "warehouse";
    }

    @GetMapping("/warehouse/{id}/enable")
    public String enableWarehouse(@PathVariable Long id){
        warehouseFacade.enableWarehouse(id);
        return "redirect:/warehouse/" + id;
    }

    @GetMapping("/warehouse/{id}/disable")
    public String disableWarehouse(@PathVariable Long id){
        warehouseFacade.disableWarehouse(id);
        return "redirect:/warehouse/" + id;
    }

    @GetMapping("/warehouse/{id}/remove")
    public String removeWarehouse(@PathVariable Long id){
        warehouseFacade.removeWarehouse(id);
        return "redirect:/listWarehouses";
    }
}
