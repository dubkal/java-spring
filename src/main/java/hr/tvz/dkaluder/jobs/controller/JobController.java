package hr.tvz.dkaluder.jobs.controller;

import hr.tvz.dkaluder.jobs.facade.WarehouseFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class JobController {

    @Autowired
    WarehouseFacade warehouseFacade;


    @RequestMapping(value = "/warehouse/uploadFile",method = RequestMethod.POST)
    public String uploadJob(@RequestParam(value = "file", required = true) MultipartFile multipartFile, @RequestParam("id") Long id) throws Exception {
        if (!multipartFile.isEmpty()){
            warehouseFacade.processUploadedJob(multipartFile, id);
        }
        return "redirect:/warehouse/" + id;
    }

    @GetMapping("/warehouse/{warehouseId}/job/{jobId}/enable")
    public String enableJob(@PathVariable("warehouseId") Long warehouseId, @PathVariable("jobId") Long jobId){
        warehouseFacade.enableJob(warehouseId, jobId);
        return "redirect:/warehouse/" + warehouseId;
    }


    @GetMapping("/warehouse/{warehouseId}/job/{jobId}/disable")
    public String disableJob(@PathVariable("warehouseId") Long warehouseId, @PathVariable("jobId") Long jobId) {
        warehouseFacade.disableJob(warehouseId, jobId);
        return "redirect:/warehouse/" + warehouseId;
    }

    @GetMapping("/warehouse/{warehouseId}/job/{jobId}/remove")
    public String removeJob(@PathVariable("warehouseId") Long warehouseId, @PathVariable("jobId") Long jobId){
        warehouseFacade.removeJob(warehouseId, jobId);
        return "redirect:/warehouse/" + warehouseId;
    }

    @GetMapping("/warehouse/{warehouseId}/job/{jobId}/run")
    public String runJob(@PathVariable("warehouseId") Long warehouseId, @PathVariable("jobId") Long jobId){
        warehouseFacade.runTalendJob(warehouseId, jobId);
        return "redirect:/warehouse/" + warehouseId;
    }
}
