package hr.tvz.dkaluder.jobs.dto;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class AddWarehouseForm {

    @NotNull
    @NotEmpty
    @Length(min = 4, max = 25)
    private String name;


    @NotNull
    @NotEmpty
    @Length(min = 4, max = 200)
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
