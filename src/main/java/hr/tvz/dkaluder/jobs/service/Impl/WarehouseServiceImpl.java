package hr.tvz.dkaluder.jobs.service.Impl;

import hr.tvz.dkaluder.jobs.model.Warehouse;
import hr.tvz.dkaluder.jobs.repository.WarehouseRepository;
import hr.tvz.dkaluder.jobs.service.WarehouseService;
import hr.tvz.dkaluder.jobs.service.Utils;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.List;

@Service
public class WarehouseServiceImpl implements WarehouseService {
    private Logger logger = LoggerFactory.getLogger(WarehouseService.class);

    private String warehousePath="/var/lib/warehouses/";

    @Autowired
    private WarehouseRepository warehouseRepository;

    @Override
    public Warehouse getWarehouseByName(String name) {
        return warehouseRepository.getByName(name);
    }

    @Override
    public void saveWarehouse(Warehouse warehouse) {
        warehouseRepository.save(warehouse);
    }

    @Override
    public List<Warehouse> getAllWarehouses() {
        return warehouseRepository.findAll();
    }

    @Override
    public Warehouse getWarehouseById(Long id) {
        return warehouseRepository.getById(id);
    }

    @Override
    public void changeWarehouseEnabled(Long id, boolean enabled) {
        Warehouse warehouse = warehouseRepository.getById(id);
        warehouse.setEnabled(enabled);
        warehouseRepository.save(warehouse);
    }

    @Override
    public void saveUploadedJob(MultipartFile multipartFile, Long id, String warehousePath, String name) {
        Warehouse warehouse = getWarehouseById(id);
        try {
            multipartFile.transferTo(new File(warehousePath + warehouse.getName() + "/jobs/" + name + ".zip"));

        } catch (Exception e){
            logger.warn("Could not save job");
        }
    }

    @Override
    public void unzipJob(Long id, String warehousePath, String name) {
        Warehouse warehouse = getWarehouseById(id);
        String source = warehousePath + warehouse.getName() + "/jobs/" + name + ".zip";
        String destination = warehousePath + warehouse.getName() + "/jobs/" + name + "/";

        try {
            ZipFile zipFile = new ZipFile(source);
            zipFile.extractAll(destination);
        } catch (ZipException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void removeWarehouse(Long id, String name) {
        warehouseRepository.deleteById(id);
        Utils.deleteDirectory(new File(warehousePath + name));
    }

}
