package hr.tvz.dkaluder.jobs.service;

import hr.tvz.dkaluder.jobs.model.Warehouse;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface WarehouseService {

    Warehouse getWarehouseByName(String name);

    void saveWarehouse(Warehouse warehouse);

    List<Warehouse> getAllWarehouses();

    Warehouse getWarehouseById(Long id);

    void changeWarehouseEnabled(Long id, boolean enabled);

    void saveUploadedJob(MultipartFile multipartFile, Long id, String warehousePath, String name);

    void unzipJob(Long id, String warehousePath, String name);

    void removeWarehouse(Long id, String name);
}
