package hr.tvz.dkaluder.jobs.service.Impl;

import hr.tvz.dkaluder.jobs.model.Job;
import hr.tvz.dkaluder.jobs.model.Warehouse;
import hr.tvz.dkaluder.jobs.repository.JobRepository;
import hr.tvz.dkaluder.jobs.repository.WarehouseRepository;
import hr.tvz.dkaluder.jobs.service.JobService;
import hr.tvz.dkaluder.jobs.service.Utils;
import org.apache.tools.ant.DirectoryScanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermission;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class JobServiceImpl implements JobService {
    private Logger logger = LoggerFactory.getLogger(JobService.class);

    @Autowired
    private WarehouseRepository warehouseRepository;

    @Autowired
    private JobRepository jobRepository;


    private String warehousePath = "/var/lib/warehouses/";

    @Override
    public void createJobForWarehouse(Long id, String name) {
        Warehouse warehouse = warehouseRepository.getById(id);
        Job job = new Job();
        job.setName(name);
        job.setEnabled(false);
        jobRepository.save(job);
        warehouse.getJobs().add(job);
        warehouseRepository.save(warehouse);
    }

    @Override
    public void setJobStatus(Long warehouseId, Long jobId, boolean status) {
        Job job = jobRepository.getOne(jobId);
        job.setEnabled(status);
        jobRepository.save(job);
    }

    @Override
    public void deleteJobFiles(Long warehouseId, Long jobId) {
        Warehouse warehouse = warehouseRepository.getById(warehouseId);
        Job job = jobRepository.getOne(jobId);
        String unzippedPath = warehousePath + warehouse.getName() + "/jobs/" + job.getName();
        String zipPath = warehousePath + warehouse.getName() + "/jobs/" + job.getName() + ".zip";
        try {
            Utils.deleteDirectory(new File(unzippedPath));
            Files.delete(Paths.get(zipPath));
        } catch (NoSuchFileException x) {
            logger.warn("File for deletion not found");
        } catch (DirectoryNotEmptyException x) {
            logger.warn("Directory for deletion is empty");
        } catch (IOException x) {
            // File permission problems are caught here.
            logger.warn("File permission error");
        }
    }

    @Override
    public void deleteJobFromDatabase(Long warehouseId, Long jobId) {
        Warehouse warehouse = warehouseRepository.getById(warehouseId);
        warehouse.getJobs().remove(jobRepository.getOne(jobId));
        warehouseRepository.save(warehouse);
        jobRepository.deleteById(jobId);
    }

    @Override
    public void runTalendJob(Long warehouseId, Long jobId) {
        try {
            Warehouse warehouse = warehouseRepository.getById(warehouseId);
            Job job = jobRepository.getById(jobId);
            String shFilePath = findShFilePath(warehouse.getName(), job.getName());
            changeFilePermissions(shFilePath);
            changeFilesPermissions(findJarFiles(warehouse.getName(), job.getName()));

            if (shFilePath != null) {
                try {
                    File file = new File(shFilePath);
                    ProcessBuilder processBuilder = new ProcessBuilder("sh", file.getAbsolutePath());
                    Process process = processBuilder.start();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        logger.info(line);
                    }
                } catch (IOException ioEx) {
                    logger.info("Io exception on sh file:" + shFilePath);
                }
            }
        } catch (Exception exception) {
            logger.info("Job not executed. WarehouseId:{} JobId:{}", warehouseId, jobId);
        }

    }

    private String findShFilePath(String warehouseName, String jobName) {
        DirectoryScanner scanner = new DirectoryScanner();
        scanner.setIncludes(new String[]{"**/*.sh"});
        scanner.setBasedir(warehousePath + warehouseName + "/jobs/" + jobName);
        scanner.setCaseSensitive(true);
        scanner.scan();
        if (scanner.getIncludedFiles().length == 1) {
            return warehousePath + warehouseName + "/jobs/" + jobName + "/" + scanner.getIncludedFiles()[0];
        }
        return null;
    }

    private List<String> findJarFiles(String warehouseName, String jobName) {
        DirectoryScanner scanner = new DirectoryScanner();
        scanner.setIncludes(new String[]{"**/*.jar"});
        scanner.setBasedir(warehousePath + warehouseName + "/jobs/" + jobName);
        scanner.setCaseSensitive(true);
        scanner.scan();
        String[] foundFiles = scanner.getIncludedFiles();
        List<String> files = new ArrayList<>();
        for (String s : foundFiles) {
            files.add(warehousePath + warehouseName + "/jobs/" + jobName + "/" + s);
        }
        return files;
    }

    private void changeFilesPermissions(List<String> files) {
        if (files != null) {
            for (String file : files) {
                changeFilePermissions(file);
            }
        }
    }

    private void changeFilePermissions(String filePath) {
        try {
            File fileTmp = new File(filePath);

            Set<PosixFilePermission> permissions = new HashSet<>();
            // Add execute permission to other group users
            permissions.add(PosixFilePermission.OWNER_EXECUTE);
            permissions.add(PosixFilePermission.GROUP_EXECUTE);
            permissions.add(PosixFilePermission.OTHERS_EXECUTE);
            permissions.add(PosixFilePermission.OWNER_READ);
            permissions.add(PosixFilePermission.GROUP_READ);
            permissions.add(PosixFilePermission.OTHERS_READ);

            // Assign permissions.
            Files.setPosixFilePermissions(Paths.get(fileTmp.getAbsolutePath()), permissions);

        } catch (IOException ioEx) {
            logger.info("Error changing file permissions");
        }
    }

}
