package hr.tvz.dkaluder.jobs.service;

public interface JobService {
    void createJobForWarehouse(Long id, String name);

    void setJobStatus(Long warehouseId, Long jobId, boolean status);

    void deleteJobFiles(Long warehouseId, Long jobId);

    void deleteJobFromDatabase(Long warehouseId, Long jobId);

    void runTalendJob(Long warehouseId, Long jobId);
}
