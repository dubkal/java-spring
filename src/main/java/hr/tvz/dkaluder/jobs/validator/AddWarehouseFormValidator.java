package hr.tvz.dkaluder.jobs.validator;

import hr.tvz.dkaluder.jobs.dto.AddWarehouseForm;
import hr.tvz.dkaluder.jobs.service.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class AddWarehouseFormValidator implements Validator {

    @Autowired
    private WarehouseService warehouseService;

    @Override
    public boolean supports(Class<?> clazz) {
        return AddWarehouseForm.class.equals(clazz);
    }

    @Override
    public void validate(Object object, Errors errors) {
        AddWarehouseForm addWarehouseForm = (AddWarehouseForm) object;

        if (warehouseService.getWarehouseByName(addWarehouseForm.getName()) != null) {
            errors.rejectValue("name", "addWarehouseForm.duplicates.name");
        }

        if (!isNameOnlyValidCharacters(addWarehouseForm.getName())){
            errors.rejectValue("name", "addWarehouseForm.character.notAllowed");
        }
    }

    private boolean isNameOnlyValidCharacters(String name){
        return name.matches("^[a-zA-Z0-9_]*$");
    }
}
