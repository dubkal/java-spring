package hr.tvz.dkaluder.jobs.converter;

import hr.tvz.dkaluder.jobs.dto.WarehouseDto;
import hr.tvz.dkaluder.jobs.model.Warehouse;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class WarehouseDtoConverter implements Converter<Warehouse, WarehouseDto> {
    @Override
    public WarehouseDto convert(Warehouse warehouse) {
        WarehouseDto warehouseDto = new WarehouseDto();
        warehouseDto.setId(warehouse.getId());
        warehouseDto.setName(warehouse.getName());
        warehouseDto.setDescription(warehouse.getDescription());
        warehouseDto.setEnabled(warehouse.isEnabled());
        if (warehouse.getJobs() != null) {
            warehouseDto.setJobs(warehouse.getJobs());
        }
        return warehouseDto;
    }
}
