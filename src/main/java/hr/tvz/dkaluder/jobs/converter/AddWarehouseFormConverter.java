package hr.tvz.dkaluder.jobs.converter;

import hr.tvz.dkaluder.jobs.dto.AddWarehouseForm;
import hr.tvz.dkaluder.jobs.model.Warehouse;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class AddWarehouseFormConverter implements Converter<AddWarehouseForm, Warehouse> {

    @Override
    public Warehouse convert(AddWarehouseForm addWarehouseForm) {
        Warehouse warehouse = new Warehouse();
        warehouse.setName(addWarehouseForm.getName());
        warehouse.setDescription(addWarehouseForm.getDescription());

        return warehouse;
    }
}
