package hr.tvz.dkaluder.jobs.facade;

import hr.tvz.dkaluder.jobs.converter.AddWarehouseFormConverter;
import hr.tvz.dkaluder.jobs.converter.WarehouseDtoConverter;
import hr.tvz.dkaluder.jobs.dto.AddWarehouseForm;
import hr.tvz.dkaluder.jobs.dto.WarehouseDto;
import hr.tvz.dkaluder.jobs.model.Job;
import hr.tvz.dkaluder.jobs.model.Warehouse;
import hr.tvz.dkaluder.jobs.service.JobService;
import hr.tvz.dkaluder.jobs.service.WarehouseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Component

public class WarehouseFacade {
    private Logger logger = LoggerFactory.getLogger(WarehouseFacade.class);

    @Autowired
    private WarehouseService warehouseService;

    @Autowired
    private JobService jobService;

    @Autowired
    private WarehouseDtoConverter warehouseDtoConverter;

    @Autowired
    private AddWarehouseFormConverter addWarehouseFormConverter;

    //todo make load from properties file
    private String warehousePath = "/var/lib/warehouses/";

    public void saveWarehouse(AddWarehouseForm addWarehouseForm) {

    try{
        Files.createDirectory(Paths.get(warehousePath + addWarehouseForm.getName() + "/"));
        Files.createDirectory(Paths.get(warehousePath + addWarehouseForm.getName() + "/files"));
        Files.createDirectory(Paths.get(warehousePath + addWarehouseForm.getName() + "/jobs"));
        Files.createDirectory(Paths.get(warehousePath + addWarehouseForm.getName() + "/logs"));
        warehouseService.saveWarehouse(addWarehouseFormConverter.convert(addWarehouseForm));

    } catch (IOException exception){
        logger.warn("Could not make warehouse directory structure");
    }

    }

    public List<WarehouseDto> getWarehouseDtos() {
        List<WarehouseDto> warehouseDtos = new ArrayList<>();
        List<Warehouse> warehouses = warehouseService.getAllWarehouses();
        for (Warehouse warehouse : warehouses) {
            warehouseDtos.add(warehouseDtoConverter.convert(warehouse));
        }
        return warehouseDtos;
    }

    public WarehouseDto getWarehouseDto(Long id) {
        return warehouseDtoConverter.convert(warehouseService.getWarehouseById(id));
    }

    public void enableWarehouse(Long id) {
        warehouseService.changeWarehouseEnabled(id, true);
    }

    public void disableWarehouse(Long id) {
        warehouseService.changeWarehouseEnabled(id, false);
    }

    public void removeWarehouse(Long id) {
        Warehouse warehouse = warehouseService.getWarehouseById(id);
        Set<Job> jobs = warehouse.getJobs();
        for(Job job : jobs){
            jobService.deleteJobFromDatabase(id, job.getId());
        }
        warehouseService.removeWarehouse(id, warehouse.getName());
    }

    public void processUploadedJob(MultipartFile multipartFile, Long id) {
        String name = new Date().toString().replaceAll(" ", "_").replaceAll(":", "_");
        jobService.createJobForWarehouse(id, name);
        warehouseService.saveUploadedJob(multipartFile, id, warehousePath, name);
        warehouseService.unzipJob(id, warehousePath, name);


    }

    public void enableJob(Long warehouseId, Long jobId) {
        jobService.setJobStatus(warehouseId, jobId, true);
    }
    public void disableJob(Long warehouseId, Long jobId){
        jobService.setJobStatus(warehouseId, jobId, false);
    }

    public void removeJob(Long warehouseId, Long jobId) {
        Warehouse warehouse = warehouseService.getWarehouseById(warehouseId);
        jobService.deleteJobFiles(warehouseId, jobId);
        jobService.deleteJobFromDatabase(warehouseId, jobId);

    }

    public void runTalendJob(Long warehouseId, Long jobId) {
        jobService.runTalendJob(warehouseId, jobId);
    }
}
