package hr.tvz.dkaluder.jobs.model;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Warehouse {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    private String description;

    private boolean enabled;

    @OneToMany
    @OrderBy(value = "id")
    private Set<Job> jobs;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Set<Job> getJobs() {
        return jobs;
    }

    public void setJobs(Set<Job> jobs) {
        this.jobs = jobs;
    }
}
