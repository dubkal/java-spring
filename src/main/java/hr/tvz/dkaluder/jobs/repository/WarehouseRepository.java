package hr.tvz.dkaluder.jobs.repository;

import hr.tvz.dkaluder.jobs.model.Warehouse;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WarehouseRepository extends JpaRepository<Warehouse, Long> {

    Warehouse getByName(String name);

    Warehouse getById(Long id);
}
