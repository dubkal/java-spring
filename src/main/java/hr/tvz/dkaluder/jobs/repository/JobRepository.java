package hr.tvz.dkaluder.jobs.repository;

import hr.tvz.dkaluder.jobs.model.Job;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JobRepository extends JpaRepository<Job, Long> {

    Job getById(Long id);
}
