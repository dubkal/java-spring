package hr.tvz.dkaluder.auth.facade;

import hr.tvz.dkaluder.auth.converter.RegistrationFormConverter;
import hr.tvz.dkaluder.auth.converter.UserDtoConverter;
import hr.tvz.dkaluder.auth.dto.RegistrationForm;
import hr.tvz.dkaluder.auth.dto.UserDto;
import hr.tvz.dkaluder.auth.enums.VerificationTokenStatus;
import hr.tvz.dkaluder.auth.model.User;
import hr.tvz.dkaluder.auth.model.VerificationToken;
import hr.tvz.dkaluder.auth.service.EmailService;
import hr.tvz.dkaluder.auth.service.SecurityService;
import hr.tvz.dkaluder.auth.service.UserService;
import hr.tvz.dkaluder.auth.service.VerificationTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserFacade {

    @Autowired
    private RegistrationFormConverter registrationFormConverter;

    @Autowired
    private UserDtoConverter userDtoConverter;

    @Autowired
    private UserService userService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private VerificationTokenService verificationTokenService;

    @Autowired
    private EmailService emailService;

    public void grantAdminRights(List<String> ids) {
        userService.grantAdminRightsFromIds(
                ids.stream().map(Long::valueOf)
                        .collect(Collectors.toList()));
    }

    public void revokeAdminRights(List<String> ids) {
        userService.revokeAdminRightsFromIds(
                ids.stream().map(Long::valueOf)
                        .collect(Collectors.toList()));
    }

    public List<UserDto> getAllUserDtos() {
        return convertUserListToUserDtoList(userService.getAllUsers());
    }

    public List<UserDto> getAllAdminDtos() {
        return convertUserListToUserDtoList(userService.getAllAdmins());
    }

    public void register(RegistrationForm registrationForm) {
        User user = registrationFormConverter.convert(registrationForm);
        userService.saveUser(user);
        user = userService.findByUsername(user.getUsername());
        VerificationToken token = verificationTokenService.createVerificationToken(user);
        verificationTokenService.saveVerificationToken(token);
        emailService.sendVerificationEmail(token);
    }

    public VerificationTokenStatus activateAccount(String token) {
        VerificationToken verificationToken = verificationTokenService.getVerificationTokenFromToken(token);
        if (verificationToken == null) {
            return null;
        }
        switch (verificationToken.getStatus()) {

            case CREATED:
                userService.activateUser(verificationToken.getUser());
                verificationToken.setStatus(VerificationTokenStatus.USED);
                verificationTokenService.saveVerificationToken(verificationToken);
                //securityService.autoLogin(verificationToken.getUser().getUsername(), verificationToken.getUser().getPassword());
                return VerificationTokenStatus.CREATED;

            case USED:
                return VerificationTokenStatus.USED;

            case EXPIRED:
                VerificationToken newVerificationToken =
                        verificationTokenService.createVerificationToken(verificationToken.getUser());
                newVerificationToken.setId(verificationToken.getId());
                verificationTokenService.saveVerificationToken(newVerificationToken);
                emailService.sendVerificationEmail(newVerificationToken);
                return VerificationTokenStatus.EXPIRED;
        }
        return null;
    }


    private List<UserDto> convertUserListToUserDtoList(List<User> users) {
        List<UserDto> userDtos = new ArrayList<>();
        for (User user : users) {
            userDtos.add(userDtoConverter.convert(user));
        }
        return userDtos;
    }
}
