package hr.tvz.dkaluder.auth.service;

import hr.tvz.dkaluder.auth.model.VerificationToken;

import javax.mail.SendFailedException;

public interface EmailService {

    void sendEmail(String mailTo, String mailFrom, String subject, String body) throws SendFailedException;

    void sendVerificationEmail(VerificationToken token);
}

