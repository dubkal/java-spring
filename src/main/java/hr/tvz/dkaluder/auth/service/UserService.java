package hr.tvz.dkaluder.auth.service;

import hr.tvz.dkaluder.auth.model.User;

import java.util.List;

public interface UserService {

    void saveUser(User user);

    User findByUsername(String username);

    List<User> getAllUsers();

    List<User> getAllAdmins();

    boolean isAdmin(User user);

    void grantAdminRights(User user);

    void revokeAdminRights(User user);

    void grantAdminRightsFromIds(List<Long> ids);

    void revokeAdminRightsFromIds(List<Long> ids);

    void activateUser(User user);
}
