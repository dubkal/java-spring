package hr.tvz.dkaluder.auth.service.Impl;

import hr.tvz.dkaluder.auth.model.VerificationToken;
import hr.tvz.dkaluder.auth.service.EmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
@PropertySource("classpath:messages.properties")
public class EmailServiceImpl implements EmailService {

    private static final Logger logger = LoggerFactory.getLogger(EmailService.class);

    @Value("${email.verification.from}")
    private String verificationMailFrom;

    @Value("${email.verification.subject}")
    private String verificationMailSubject;

    @Value("${email.verification.body}")
    private String verificationMailBody;

    @Value("${email.verification.link}")
    private String verificationMailLink;

    @Autowired
    private JavaMailSender emailSender;

    @Override
    public void sendEmail(String mailTo, String mailFrom, String subject, String body) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(mailTo);
        message.setFrom(mailFrom);
        message.setSubject(subject);
        message.setText(body);
        emailSender.send(message);
    }

    @Override
    public void sendVerificationEmail(VerificationToken token) {
        String mailTo = token.getUser().getEmail();
        String mailFrom = verificationMailFrom;
        String subject = verificationMailSubject;
        String body = verificationMailBody + "\n" + verificationMailLink + token.getToken();
        sendEmail(mailTo, mailFrom, subject, body);
        /* todo change logger when email intercept works */
        logger.info(mailTo + "\n" + mailFrom + "\n" + subject + "\n" + body);
    }
}
