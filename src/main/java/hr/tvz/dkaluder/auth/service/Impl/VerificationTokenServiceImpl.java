package hr.tvz.dkaluder.auth.service.Impl;

import hr.tvz.dkaluder.auth.enums.VerificationTokenStatus;
import hr.tvz.dkaluder.auth.model.User;
import hr.tvz.dkaluder.auth.model.VerificationToken;
import hr.tvz.dkaluder.auth.repository.VerificationTokenRepository;
import hr.tvz.dkaluder.auth.service.VerificationTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class VerificationTokenServiceImpl implements VerificationTokenService {

    @Autowired
    private VerificationTokenRepository verificationTokenRepository;

    @Override
    public void saveVerificationToken(VerificationToken verificationToken) {
        verificationTokenRepository.save(verificationToken);
    }

    @Override
    public VerificationToken getVerificationTokenFromToken(String token) {
        return verificationTokenRepository.findByToken(token);
    }

    @Override
    public VerificationToken createVerificationToken(User user) {
        VerificationToken verificationToken = new VerificationToken();
        verificationToken.setUser(user);
        verificationToken.setExpiryDate(createExpiryDate());
        verificationToken.setToken(UUID.randomUUID().toString());
        verificationToken.setStatus(VerificationTokenStatus.CREATED);
        return verificationToken;
    }

    @Override
    public int setExpiredTokenStatus(){
        List<VerificationToken> expiredTokens =
                verificationTokenRepository.getAllByExpiryDateBeforeAndStatusIs(new Date(), VerificationTokenStatus.CREATED);
        expiredTokens.forEach(verificationToken -> verificationToken.setStatus(VerificationTokenStatus.EXPIRED));
        verificationTokenRepository.saveAll(expiredTokens);
        return expiredTokens.size();
    }

    private Date createExpiryDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_WEEK, 1);
        return calendar.getTime();
    }
}
