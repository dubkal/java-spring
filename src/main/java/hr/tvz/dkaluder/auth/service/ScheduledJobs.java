package hr.tvz.dkaluder.auth.service;

import org.springframework.scheduling.annotation.Scheduled;

public interface ScheduledJobs {

    void setExpiredTokenStatus();
}
