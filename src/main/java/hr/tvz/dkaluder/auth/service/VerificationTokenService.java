package hr.tvz.dkaluder.auth.service;

import hr.tvz.dkaluder.auth.model.User;
import hr.tvz.dkaluder.auth.model.VerificationToken;

public interface VerificationTokenService {


    void saveVerificationToken(VerificationToken verificationToken);

    VerificationToken getVerificationTokenFromToken(String token);

    VerificationToken createVerificationToken(User user);

    int setExpiredTokenStatus();
}
