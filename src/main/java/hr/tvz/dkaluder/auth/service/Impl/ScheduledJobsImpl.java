package hr.tvz.dkaluder.auth.service.Impl;

import hr.tvz.dkaluder.auth.service.ScheduledJobs;
import hr.tvz.dkaluder.auth.service.VerificationTokenService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduledJobsImpl implements ScheduledJobs {

    private Logger logger = LoggerFactory.getLogger(ScheduledJobsImpl.class);

    @Autowired
    private VerificationTokenService verificationTokenService;

    @Override
    @Scheduled(fixedRateString = "${verification.token.cleanup.rate}", initialDelay = 120000)
    public void setExpiredTokenStatus() {

        int numberOfExpiredTokens = verificationTokenService.setExpiredTokenStatus();
        logger.debug("Token cleanup  has set expired status on {} tokens", numberOfExpiredTokens);

    }
}
