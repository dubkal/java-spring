package hr.tvz.dkaluder.auth.service.Impl;

import hr.tvz.dkaluder.auth.enums.RoleEnum;
import hr.tvz.dkaluder.auth.model.Role;
import hr.tvz.dkaluder.auth.model.User;
import hr.tvz.dkaluder.auth.repository.RoleRepository;
import hr.tvz.dkaluder.auth.repository.UserRepository;
import hr.tvz.dkaluder.auth.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public void saveUser(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setRoles(new HashSet<>());
        user.getRoles().add(roleRepository.getRoleByName(RoleEnum.ROLE_USER.toString()));
        userRepository.save(user);
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAllByEnabledTrueAndRolesNotContains(
                roleRepository.getRoleByName(RoleEnum.ROLE_ADMIN.toString()));
    }

    @Override
    public List<User> getAllAdmins() {
        return userRepository.findAllByEnabledTrueAndRolesContains(
                roleRepository.getRoleByName(RoleEnum.ROLE_ADMIN.toString()));
    }

    @Override
    public boolean isAdmin(User user) {
        for (Role role : user.getRoles()) {
            if (role.getName().equals(RoleEnum.ROLE_ADMIN.toString()))
                return true;
        }
        return false;
    }

    @Override
    public void grantAdminRights(User user) {
        user.getRoles().add(roleRepository.getRoleByName(RoleEnum.ROLE_ADMIN.toString()));
    }

    @Override
    public void revokeAdminRights(User user){
        user.getRoles().remove(roleRepository.getRoleByName(RoleEnum.ROLE_ADMIN.toString()));
    }

    @Override
    public void grantAdminRightsFromIds(List<Long> ids) {
        List<User> users = userRepository.findAllById(ids);
        for (User user : users) {
            grantAdminRights(user);
        }
        userRepository.saveAll(users);
    }

    @Override
    public void revokeAdminRightsFromIds(List<Long> ids) {
        List<User> users = userRepository.findAllById(ids);
        for (User user : users) {
            revokeAdminRights(user);
        }
        userRepository.saveAll(users);
    }


    @Override
    public void activateUser(User user) {
        user.setEnabled(true);
        userRepository.save(user);
    }
}
