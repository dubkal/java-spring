package hr.tvz.dkaluder.auth.service;

public interface SecurityService {

    String findLoggedInUsername();

    void autoLogin(String username, String password);
}
