package hr.tvz.dkaluder.auth.model;

import hr.tvz.dkaluder.auth.enums.VerificationTokenStatus;

import javax.persistence.*;
import java.util.Date;

@Entity
public class VerificationToken {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String token;

    @OneToOne(targetEntity = User.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "users_id")
    private User user;

    private Date expiryDate;

    @Enumerated(EnumType.STRING)
    private VerificationTokenStatus status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public VerificationTokenStatus getStatus() {
        return status;
    }

    public void setStatus(VerificationTokenStatus status) {
        this.status = status;
    }
}