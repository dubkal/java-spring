package hr.tvz.dkaluder.auth.repository;

import hr.tvz.dkaluder.auth.enums.VerificationTokenStatus;
import hr.tvz.dkaluder.auth.model.VerificationToken;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface VerificationTokenRepository extends JpaRepository<VerificationToken, Long> {

    VerificationToken findByToken(String token);

    //@Query(value = "from VerificationToken t where expiryDate < :date and status = :status")
    List<VerificationToken> getAllByExpiryDateBeforeAndStatusIs(Date date, VerificationTokenStatus status);

}
