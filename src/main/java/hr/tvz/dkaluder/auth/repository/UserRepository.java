package hr.tvz.dkaluder.auth.repository;

import hr.tvz.dkaluder.auth.model.Role;
import hr.tvz.dkaluder.auth.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsername(String username);

    List<User> findAllByEnabledTrueAndRolesContains(Role role);

    List<User> findAllByEnabledTrueAndRolesNotContains(Role role);
}
