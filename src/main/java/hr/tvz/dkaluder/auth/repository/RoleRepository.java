package hr.tvz.dkaluder.auth.repository;

import hr.tvz.dkaluder.auth.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Role getRoleByName(String name);
}
