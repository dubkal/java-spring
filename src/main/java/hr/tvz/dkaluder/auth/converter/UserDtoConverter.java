package hr.tvz.dkaluder.auth.converter;

import hr.tvz.dkaluder.auth.dto.UserDto;
import hr.tvz.dkaluder.auth.model.User;
import hr.tvz.dkaluder.auth.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class UserDtoConverter implements Converter<User, UserDto> {

    @Autowired
    private UserService userService;

    @Override
    public UserDto convert(User user) {
        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setUsername(user.getUsername());
        userDto.setEmail(user.getEmail());
        return userDto;
    }
}
