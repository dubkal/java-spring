package hr.tvz.dkaluder.auth.converter;

import hr.tvz.dkaluder.auth.dto.RegistrationForm;
import hr.tvz.dkaluder.auth.model.User;
import hr.tvz.dkaluder.auth.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class RegistrationFormConverter implements Converter<RegistrationForm, User> {

    @Autowired
    RoleRepository roleRepository;

    @Override
    public User convert(RegistrationForm registrationForm) {
        User user = new User();
        user.setUsername(registrationForm.getUsername());
        user.setPassword(registrationForm.getPassword());
        user.setEmail(registrationForm.getEmail());

        return user;
    }
}
