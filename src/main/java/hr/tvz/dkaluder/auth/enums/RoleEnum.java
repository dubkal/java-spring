package hr.tvz.dkaluder.auth.enums;

public enum RoleEnum {
    ROLE_ADMIN, ROLE_USER;
}
