package hr.tvz.dkaluder.auth.enums;

public enum VerificationTokenStatus {
    CREATED, USED, EXPIRED;
}
