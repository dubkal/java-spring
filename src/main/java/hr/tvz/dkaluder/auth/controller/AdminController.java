package hr.tvz.dkaluder.auth.controller;

import hr.tvz.dkaluder.auth.facade.UserFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class AdminController {

    @Autowired
    private UserFacade userFacade;


    @GetMapping("/grantAdmins")
    public String grantAdmins(Model model) {
        model.addAttribute("userDtos", userFacade.getAllUserDtos());

        return "grantAdmins";
    }

    @PostMapping("/grantAdminsAction")
    public String grantAdmins(Model model, @RequestParam(required = false) List<String> grantedAdminIds) {
        if (grantedAdminIds != null) {
            userFacade.grantAdminRights(grantedAdminIds);
        }
        return "redirect:/grantAdmins";
    }

    @GetMapping("/revokeAdmins")
    public String revokeAdmins(Model model) {
        model.addAttribute("userDtos", userFacade.getAllAdminDtos());

        return "revokeAdmins";
    }

    @PostMapping("/revokeAdminsAction")
    public String revokeAdmins(Model model, @RequestParam(required = false) List<String> revokedAdminIds) {
        if (revokedAdminIds != null) {
            userFacade.revokeAdminRights(revokedAdminIds);
        }
        return "redirect:/revokeAdmins";
    }
}
