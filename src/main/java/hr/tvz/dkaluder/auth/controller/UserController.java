package hr.tvz.dkaluder.auth.controller;

import hr.tvz.dkaluder.auth.enums.VerificationTokenStatus;
import hr.tvz.dkaluder.auth.dto.RegistrationForm;
import hr.tvz.dkaluder.auth.facade.UserFacade;
import hr.tvz.dkaluder.auth.validator.RegistrationFormValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

@Controller
public class UserController {

    @Autowired
    private RegistrationFormValidator registrationFormValidator;

    @Autowired
    private UserFacade userFacade;


    @GetMapping("/registration")
    public String registration(Model model) {
        model.addAttribute("registrationForm", new RegistrationForm());

        return "registration";
    }

    @PostMapping("/registration")
    public String registration(@ModelAttribute("registrationForm") @Valid RegistrationForm registrationForm,
                               BindingResult bindingResult, RedirectAttributes  redirectAttributes) {
        registrationFormValidator.validate(registrationForm, bindingResult);
        if (bindingResult.hasErrors()) {
            return "registration";
        }
        userFacade.register(registrationForm);
        redirectAttributes.addFlashAttribute("activateAccount", "true");
        return "redirect:/login";
    }

    @GetMapping("/login")
    public String login(Model model, String error, String logout) {
        if (error != null) {
            model.addAttribute("error", true);
        }
        if (logout != null) {
            model.addAttribute("logout", true);
        }
        return "login";
    }

    @GetMapping({"/", "/welcome"})
    public String welcome(Model model) {
        return "welcome";
    }


    @GetMapping("/confirmEmail/{token}")
    public String confirmEmail(@PathVariable("token") String token, RedirectAttributes redirectAttributes) {
        VerificationTokenStatus status = userFacade.activateAccount(token);
        if (status == null) {
            return "redirect:/login";
        }
        switch (status) {
            case CREATED:
                redirectAttributes.addFlashAttribute("tokenActivated", "true");
                break;
            case USED:
                redirectAttributes.addFlashAttribute("tokenUsed", "true");
                break;
            case EXPIRED:
                redirectAttributes.addFlashAttribute("tokenExpired", "true");
                break;
        }
        return "redirect:/login";
    }
}