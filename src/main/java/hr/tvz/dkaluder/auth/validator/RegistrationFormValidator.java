package hr.tvz.dkaluder.auth.validator;

import hr.tvz.dkaluder.auth.dto.RegistrationForm;
import hr.tvz.dkaluder.auth.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class RegistrationFormValidator implements Validator {

    @Autowired
    private UserService userService;

    @Override
    public boolean supports(Class<?> clazz){
        return RegistrationForm.class.equals(clazz);
    }

    @Override
    public void validate(Object object, Errors errors){
        RegistrationForm registrationForm = (RegistrationForm) object;

        if (userService.findByUsername(registrationForm.getUsername()) != null) {
            errors.rejectValue("username", "registrationForm.duplicate.username");
        }

        if (!registrationForm.getConfirmPassword().equals(registrationForm.getPassword())) {
            errors.rejectValue("confirmPassword", "registrationForm.notMatching.password");
        }
    }

}
